/usr/bin/env groovy

def call(Map pipelineParams) {
def analytics_base_uri = "https://analytics-ingest.anypoint.mulesoft.com"
def base_uri = "https://anypoint.mulesoft.com"
def REGN = "us-east-1"
def server = Artifactory.server 'Toolchain-Artifactory'
def rtMaven = Artifactory.newMavenBuild()
def buildInfo
rtMaven.deployer releaseRepo: 'adc-de-v3-mulesoft-release', snapshotRepo: 'adc-de-v3-mulesoft-snapshot', server: server

pipeline {
    agent { label 'ADC_V3_DEV'}
     
	 environment {
        ENVIRONMENT_NAME = "dev"
		AUTO_DISCOVERY = getAutoDiscoveryId(pipelineParams)
		API_NAME = getAPIName(pipelineParams)
		analytics_base_uri = "https://analytics-ingest.anypoint.mulesoft.com"
		base_uri = "https:/anypoint.mulesoft.com"
	 }
	 tools {
		jdk "JDK-8u212"
		//maven "Maven-3-6"
	 }
    stages {
	
		
		
		stage('Execute Maven') {
			
			options { skipDefaultCheckout(true) }
			
			when { 
				anyOf {
                    branch "development";					
					branch "release/*"
                }
				 
			}

				steps{
						script {
								echo "${ENVIRONMENT_NAME}"
								echo "${AUTO_DISCOVERY}"
							// Define maven goal
								rtMaven.deployer.deployArtifacts = false
								buildInfo = rtMaven.run pom: 'pom.xml', goals: 'clean install'
						}
				}
		}
		
		stage ('Get bearer token & depoy') {
			
			options { skipDefaultCheckout(true) }			
			when { 
				anyOf {
					branch "development";
                    branch "release/*"
                }
				 
			}

				steps {
					script {
                        b_token = sh(script: "curl -d 'username=test_viren&password=Admin@123' https://anypoint.mulesoft.com/accounts/login | jq -r .access_token", returnStdout: true).trim()
						package_name = sh(script: "ls ./target | grep adc-", returnStdout: true)
					}
    		}		
		}			
		stage ('check api status & Deploy') {
			
			options { skipDefaultCheckout(true) }			
			when { 
				anyOf {
					branch "development";
                    branch "release/*"
                }
				 
			}

				steps {
						script {
						
							status = sh(script: "curl -k -X GET -H 'authorization: Bearer ${b_token}' -H 'x-anypnt-env-id: 8029e000-6480-47b9-9c44-df11e9fda4a5' -H 'x-anypnt-org-id: 2c74da8e-411a-4100-804a-000ae178fa13' https://anypoint.mulesoft.com/cloudhub/api/v2/applications/${API_NAME}-${ENVIRONMENT_NAME} | jq .status", returnStdout: true).trim()
							
							if ("${status}"=="404"){
								Create_New_API()
								
							}else{
								Update_API()
							}
						}
				}
        }
		
		
		
	}
	post {
			always { 
               
              	emailext (
					attachLog: true,
					subject: "JENKINS: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
					body:'${SCRIPT,template="groovy-html.template"}',
					compressLog: true,
					mimeType: 'text/html',
					recipientProviders: [[$class: 'DevelopersRecipientProvider']],
					to: 'viren-shah.shah@capgemini.com'
				)
			}

	}
	 
}
}
def Create_New_API(){

	sh "curl -X POST https://anypoint.mulesoft.com/cloudhub/api/v2/applications -H 'authorization: bearer ${b_token}' -H 'cache-control: no-cache,no-cache' -H 'x-anypnt-env-id: 8029e000-6480-47b9-9c44-df11e9fda4a5' -H 'x-anypnt-org-id: 2c74da8e-411a-4100-804a-000ae178fa13' -F 'appInfoJson={\"domain\":\"${API_NAME}-${ENVIRONMENT_NAME}\",\"muleVersion\" : {\"version\":\"4.2.2\"},\"region\" : \"us-east-1\",\"monitoringEnabled\":true,\"monitoringAutoRestart\" :true, \"workers\": {\"amount\":1, \"type\": {\"name\":\"Micro\",\"weight\":\"0.1\",\"cpu\":\"0.1 vCores\", \"memory\":\"500 MB memory\"}}, \"loggingNgEnabled\":true, \"persistentQueues\":false, \"properties\":{\"anypoint.platform.analytics_base_uri\":\"${analytics_base_uri}\", \"encryption.key\":\"${encrypt_key}\", \"anypoint.platform.client_id\":\"${client_id}\", \"anypoint.platform.client_secret\":\"${client_secret}\",\"anypoint.platform.base_uri\":\"https://anypoint.mulesoft.com\", \"anypoint.platform.config.analytics.agent.enabled\":\"true\", \"autodiscoveryid\":\"${AUTO_DISCOVERY}\", \"env\":\"${ENVIRONMENT_NAME}\", \"elk_vpc_endpoint\":\"${ELK_ENDPOINT}\", \"analytics_base_uri\":\"${base_uri}\"}}' -F autoStart=true -F file=@target/'$package_name'"

}

def Update_API(){

	sh "curl -X POST https://anypoint.mulesoft.com/cloudhub/api/v2/applications/${API_NAME}-${ENVIRONMENT_NAME}/files -H 'authorization: Bearer ${b_token}' -H 'cache-control: no-cache' -H 'x-anypnt-env-id: 8029e000-6480-47b9-9c44-df11e9fda4a5' -H 'x-anypnt-org-id: 2c74da8e-411a-4100-804a-000ae178fa13' -F file=@target/'${package_name}'"
}

def getAPIName(Map pipelineParams){

    return pipelineParams.apiName

	
}

def getAutoDiscoveryId(Map pipelineParams){

    return pipelineParams.autoDiscovery

}